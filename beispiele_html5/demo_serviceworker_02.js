// Abgeänderte Version von https://developers.google.com/web/fundamentals/primers/service-workers/

const CACHE_NAME = 'my-site-cache-v1'
let urlsToCache = ['demo_serviceworker_02.html']

self.addEventListener('install', function (event) {
  console.log('Service Worker Install-Phase')

  event.waitUntil(
    caches.open(CACHE_NAME).then((cache) => {
      console.log('Cache ist nun offen und wird gefüllt')
      return cache.addAll(urlsToCache).then(() => {
        self.skipWaiting()
      })
    })
  )
})

self.addEventListener('activate', function (event) {
  console.log('Service Worker Activate-Phase')

  // Sofort aktivieren
  event.waitUntil(clients.claim())
})

self.addEventListener('fetch', function (event) {
  console.log('Neuer Request: ' + event.request.url)

  // IMPORTANT: Clone the request. A request is a stream and
  // can only be consumed once. Since we are consuming this
  // once by cache and once by the browser for fetch, we need
  // to clone the response.
  let fetchRequest = event.request.clone()
  event.respondWith(
    fetch(event.request)
      .then((response) => {
        // Nur valide Antworten cachen
        if (!response || response.status !== 200 || (response.type !== 'basic' && response.type !== 'cors')) {
          console.log('Response ist Fehlerhaft und wird nicht gecached')
          return response
        }

        // IMPORTANT: Clone the response. A response is a stream
        // and because we want the browser to consume the response
        // as well as the cache consuming the response, we need
        // to clone it so we have two streams.
        let responseToCache = response.clone()

        // Anfrage Cachen
        caches.open(CACHE_NAME).then((cache) => {
          cache.put(fetchRequest, responseToCache)
          console.log('Request ist nun im Cache')
        })

        return response
      })
      .catch(() => {
        console.log('Request failed, versuche mein Glück im Cache...')
        return caches.match(event.request).then((response) => {
          if (response) {
            console.log('Guuut wir haben was im Cache')
            return response
          }

          // Leider nichts im Cache, eine 404-Response zurückgeben
          return new Response(null, {
            status: 404,
            statusText: 'Fetch failed via Service Worker',
          })
        })
      })
  )
})
