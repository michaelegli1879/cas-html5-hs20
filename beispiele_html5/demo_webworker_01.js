// Nachrichten der Webseite entgegennehmen
self.addEventListener('message', function (message) {
  self.postMessage('Going to count to ' + message.data)
  doSomeWork(message.data)
  self.postMessage('Finished my work')
})

let doSomeWork = function (loopend) {
  for (i = 0; i < loopend; i++) {
    // work...
  }
}
