// Service Worker
self.addEventListener('install', function (event) {
  console.log('Service Worker Install-Phase')

  // Falls schon ein Service Worker aktiv ist, wird der neue erst aktiviert wenn der
  // alte nicht mehr gebraucht wird (also alle Tabs geschlossen sind)
  // Der neue Worker kann jedoch auch sofort aktiviert werden:
  self.skipWaiting()
})

self.addEventListener('activate', function (event) {
  console.log('Service Worker Activate-Phase')

  // Alle aktiven Clients sofort übernehmen
  // Dies löst bei den CLients den controllerchange-Event aus
  event.waitUntil(clients.claim())
})

self.addEventListener('message', function (event) {
  // Message an alle Clients senden
  self.clients.matchAll().then((clients) => {
    clients.forEach((client) => client.postMessage('Going to count to ' + event.data))

    // Work starten
    doSomeWork(event.data)

    // Und nochmals eine Message an alle...
    clients.forEach((client) => client.postMessage('Finished my work'))
  })

  // Nur dem sendenden Client eine Message zu senden ist etwas einfacher:
  //event.source.postMessage("Foo")
})

let doSomeWork = function (loopend) {
  for (i = 0; i < loopend; i++) {
    // work...
  }
}
